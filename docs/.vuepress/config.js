module.exports = {
    title: 'Simple UI',
    description: '一个基于Django Admin的现代化主题',
    head: [ // 注入到当前页面的 HTML <head> 中的标签
        ['link', {rel: 'icon', href: '/images/logo.png'}],
        ['link', {rel: 'manifest', href: '/images/logo.png'}],
        ['link', {rel: 'apple-touch-icon', href: '/images/logo.png'}],
        ['meta', {'http-quiv': 'pragma', cotent: 'no-cache'}],
        ['meta', {'http-quiv': 'pragma', cotent: 'no-cache,must-revalidate'}],
        ['meta', {'http-quiv': 'expires', cotent: '0'}],
        ["script", { async: true, src: "https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-9423996531680870", crossorigin: "anonymous" }]
    ],
    serviceWorker: true, // 是否开启 PWA
    base: '/docs/simpleui/', // 部署到github相关的配置
    markdown: {
        lineNumbers: true // 代码块是否显示行号
    },
    themeConfig: {
        nav: [ // 导航栏配置
            {text: '付费定制&有偿帮助', link: '/custom'},
            {text: '指南', link: '/'},
            {text: '介绍', link: '/doc'},
            {text: '教程&指南', link: '/quick'},
            {text: '社区&官网', link: 'https://simpleui.88cto.com'},
            {text:'友情链接',
                items:[
                    {text: 'SimpleUI专业版', link: 'https://simpleui.72wo.com/simplepro'},
                    {text: 'Django-Vue-Admin', link: 'https://gitee.com/liqianglog/django-vue-admin'}
                ]
            },
            {text: 'Simple Pro 专业版', link: 'https://simpleui.72wo.com/simplepro'}
            // {text: '在线Demo', link: 'http://simplepro.demo.88cto.com'}
        ],
        sidebar: 'auto', // 侧边栏配置
        sidebarDepth: 3
    },
    locales: {
        // 每个语言对象的键(key)，是语言的访问路径。
        // 然而，一种特例是将 '/' 作为默认语言的访问路径。
        '/': {
            lang: 'zh-hans', // 这个值会被设置在 <html> 的 lang 属性上
            title: 'Simple UI',
            description: '一个基于Django Admin的现代化主题',
            label: '简体中文',
            selectText: '选择语言'
        },
        '/en/': {
            lang: 'en-US',
            title: 'Simple UI',
            description: 'A modern theme based on vue+element-ui for django admin',
            label: 'English',
            selectText: 'Languages'
        }
    }
};