---
home: true
heroImage: /images/主页.png
actionText: GO →
actionLink: /en/doc.html
features:
- title: Cool
  details: Built in 28 popular themes
- title: Easy
  details: Simple configuration, get started quickly, add simpleui in settings.py and start it immediately, the efficiency is increased by 100%! Make back-end development handy.
- title: Fast
  details:  pip lightning installation, 100% compatible with native, admin without modifying the code
footer: Copyright © 2019-present Simple Forum
---

## Website
[https://simpleui.88cto.com/simpleui/en/](https://simpleui.88cto.com/simpleui/en/)

## Forum
[https://simpleui.88cto.com](https://simpleui.88cto.com)


## Install

``` shell
pip3 install django-simpleui
```