---
home: true
heroImage: /images/主页.png
actionText: 快速上手 →
actionLink: doc.html
features:
- title: 简洁至上
  details: 以 Django Admin 为基础，配合Element-UI和Vue，让页面更具现代化。
- title: Vue驱动
  details: 享受 Vue +Element-UI 的开发体验，在项目中使用 Vue 组件，带来高效开发。
- title: 高效率
  details: 得益于良好的Django生态环境，配合Simple UI让各种项目快速交付。
footer: Copyright © 2019-present Simple社区
---

## 官网
[https://simpleui.88cto.com/simpleui](https://simpleui.88cto.com/simpleui)

## 社区
[https://simpleui.88cto.com](https://simpleui.88cto.com)

如果您遇到使用的问题，欢迎到社区进行提问。

## 快速安装

``` shell
pip3 install django-simpleui
```